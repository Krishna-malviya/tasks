from tkinter import *
from PIL import Image,ImageTk
import datetime
from tkinter import messagebox
import playsound

window = Tk()
window.config(bg='black')

frame = Frame(window,bg='black')
frame.grid(row=0,column=0,padx=100,pady=40)

alarm=None
# alarm1=None
# alarm2=None

stop = False
showtime = StringVar()

def beep():
    playsound.playsound('wakeup.mp3')

def show_time():
    now = datetime.datetime.now()
    showtime.set(now.strftime('%B %d, %y %I:%M:%S'))
    if alarm == now.strftime('%I:%M') and stop == False:
        beep()
    # if alarm1 == now.strftime('%I:%M') and stop == False:
    #     beep()
    # if alarm2 == now.strftime('%I:%M') and stop == False:
    #     beep()
    l3.after(1000,show_time)

def set_alarm():
    time = e1.get()
    print(e1.get())
    l6 = Label(frame,bg='black',foreground='white',font=('Montsarat','25'),text=e1.get())
    l6.grid(row=1,column=2,padx=100)
    global alarm
    # global alarm1
    # global alarm2
    global stop
    stop = False
    if ':' in e1.get():
        alarm = time
        e1.delete(0,len(alarm))
        # alarm1 = time
        # e1.delete(0,len(alarm1))
        # alarm2 = time
        # e1.delete(0,len(alarm2))
    else:
        messagebox.showerror('Error','Wrong Input!!!!!!')

def stop_alarm():
    global stop
    stop = True

l1=Label(frame,text="Alarm Clock",font=('Cooper Black','25'),bg='black',foreground='white')
l1.grid(row=0,column=0,columnspan=2,pady=10,sticky=EW)

img = Image.open('alarmclock.png').resize((200,200))
photo = ImageTk.PhotoImage(img)
l2 = Label(frame,image=photo,bg='black')
l2.grid(row=1,column=0,columnspan=2,pady=10)

l3 = Label(frame,text='Time show',textvariable=showtime,bg='black',font=('Montsarat','15'),foreground='white')
l3.grid(row=2,column=0,columnspan=2,pady=10,sticky=EW)
show_time()

l4= Label(frame,text='Set Alarm',bg='black',font=('Montsarat','15'),foreground='white')
l4.grid(row=3,column=0,pady=10)

e1 = Entry(frame,font=('Motsarat','15'),width=10)
e1.grid(row=3,column=1,pady=10)

b1 = Button(frame,text='Set Alarm',bg='skyblue',font=('Montsarat','15'),bd=0,command=set_alarm)
b1.grid(row=4,column=0,pady=10,padx=15)

b2 = Button(frame,text='Stop Alarm',bg='skyblue',font=('Montsarat','15'),bd=0,command=stop_alarm)
b2.grid(row=4,column=1,pady=10,padx=15)

l5 = Label(frame,text='Show Alarms',bg='black',font=('Montsarat','20'),foreground='white')
l5.grid(row=0,column=2,padx=80)

# l6 = Label(frame,bg='black',foreground='white',font=('Montsarat','25'),text=e1.get(),textvariable=alarmtime)
# l6.grid(row=1,column=2,padx=100)

window.title('My Alarm CLock')
window.geometry('700x500')
window.resizable(False,False)
window.mainloop()
